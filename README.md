# CppVisualTracing

#### 介绍

cpp可视化性能测试

#### 软件架构

traing tool 提供插入测试点方法 使用

- TRACING_VISUAL_B
- TRACING_VISUAL_E
  即可插入过程开始标记和过程结束标记，生成的json文件会自动记录同过程的开始和结束时间。

tracing loader 用于加载 tracing tool 生成的json文件，并渲染为网页进行显示。

参照main.cppg给出的例子，运行结束后生成json文件，使用命令

```shell
tracing_loader -f path/to/result.json
```
加载指定的json文件，即可完成可视化。

#### 构建

本项目采用 xmake 进行构建，

```shell
cd <projet_dir>
xmake build
```
等待完成依赖安装并构建完毕。
#### 查看效果

将`resource`拷贝至应用程序生成目录
```shell
#windows平台需要自行将/改变为\
tracing_loader -f ./result.json
```
稍后，程序将会为你自动打开浏览器，并进行如下展示。
![img.png](img.png)
画面上的彩色小条的长度代表实际执行时间。