#include "tracing/tracing.h"
#include <iostream>
#include <thread>

using namespace std;

// 在代码中插入开始点结束点
// 生成tracing json文件
// 使用 tracing loader 进行可视化
int main(int argc, char **argv) {

  cpp_visual::TracingTool::setTracingConf(cpp_visual::TracingConfig{true, "./result.json", 5});
  //cpp_visual::TracingTool::loadTracingConf("tracingconf.json");

  // 使用宏
  {
    // 任务名 线程名 过程名 创建开始点
    TRACING_VISUAL_B(MAIN, MAIN_THREAD, READY);
    std::this_thread::sleep_for(std::chrono::milliseconds(40));
  }

  // 自己创建
  cpp_visual::TracingDuration duration("Main", "main_thread", "hello");
  duration.begin();
  cout << "hello world!" << endl;
  std::this_thread::sleep_for(std::chrono::milliseconds(20));
  cpp_visual::TracingDuration duration2("Main", "main_thread", "hello2");
  duration2.begin();
  std::this_thread::sleep_for(std::chrono::milliseconds(20));
  duration2.end();
  duration.end();

  TRACING_VISUAL_B(MAIN, MAIN_THREAD, WORLD);
  std::this_thread::sleep_for(std::chrono::milliseconds(20));
  TRACING_VISUAL_E(MAIN, MAIN_THREAD, WORLD);

  // 测试开始和结束不在一个作用域也可以
  { TRACING_VISUAL_E(MAIN, MAIN_THREAD, READY); } // 创建结束点

  cpp_visual::TracingTool::finish();
  return 0;
}
