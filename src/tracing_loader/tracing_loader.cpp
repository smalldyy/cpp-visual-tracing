//
// Created by smalldy on 2022/6/7.
//
#include <CLI/CLI.hpp>
#include <filesystem>
#include <httplib.h>
#include <iostream>

using namespace std;
// color
// https://github.com/catapult-project/catapult/blob/master/tracing/tracing/base/color_scheme.html

int main(int argc, char **argv) {
  CLI::App app("tracing loader command line tool");
  // app.add_flag("-h,--help", "print this help")->configurable(false);
  std::string file;
  app.add_option("-f,--file", file, "the tracing json file to load")
      ->capture_default_str()
      ->run_callback_for_default()
      ->check(CLI::ExistingFile);

  CLI11_PARSE(app, argc, argv);

  if (app.get_option("--help")
          ->as<bool>()) { // NEW: print configuration and exit
    std::cout << app.config_to_str(true, false);
    return 0;
  }

  if (!file.empty()) {
    cout << "the tracing file = \t" << file << std::endl;
#if OS_WINDOWS
    system("start http://localhost:8081/tracingtool.html");
    cout << "exec = \t"
         << "start http://localhost:8081/tracingtool.html" << std::endl;
#elif OS_LINUX
    system("xdg-open http://localhost:8081/tracingtool.html");
    cout << "exec = \t"
         << "xdg - open http://localhost:8081/tracingtool.html" << std::endl;
#endif
    if (std::filesystem::exists("./resource/tracing.json")) {
      std::filesystem::remove("./resource/tracing.json");
    }
    std::filesystem::copy_file(file, "./resource/tracing.json");
  }

  httplib::Server server;
  server.set_mount_point("/", "./resource");
  server.listen("0.0.0.0", 8081);

  return 0;
}
