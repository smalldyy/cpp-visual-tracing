-- @formatter:off

add_rules("mode.debug", "mode.release")

set_languages("c++17")
add_requires("cli11", "cpp-httplib", "nlohmann_json")

target("tracingloader")
    set_kind("binary")
    add_files("src/tracing_loader/*.cpp", "src/tracing/*.cpp")
    add_packages("cli11", "cpp-httplib", "nlohmann_json")
    if is_plat("linux") then
        add_links("pthread")
    end

    if is_plat("windows") then
        add_defines("OS_WINDOWS")
    end

    if is_plat("linux") then
        add_defines("OS_LINUX")
    end

target("test")
    set_kind("binary")
    add_files("src/tracing/*.cpp")
    add_files("src/main.cpp")
    add_packages("nlohmann_json")
    if is_plat("linux") then
        add_links("pthread")
    end
